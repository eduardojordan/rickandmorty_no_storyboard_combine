# RickAndMorty No storyboard + Combine

![images](Capturas.jpg)



Its a practice of iOS app with petition APIRest of the RickAndMorty API

# Build
- Xcode 13.4.1 
- iOS  15.5
- Device: iPhone 13

# Software used
- Xcode
- SourceTree
- Swift
- Icon Set Creator
- Terminal
- Postman

## Include
- Swift Language
- Include Localization
- Include URLSession
- Include error handler
- Iclude Coordinators 
- Include SearchBar
- Loader
- Reacability

# Languaje Localization
- Spanish
- English

# Frameworks 
- UIKit

# Architecture Pattern
MVVM - Model View ViewModel
Combine framework

# Unit Test
Yes

# Development and decision process
Prepare a mockup and simple icons to understand the needs of the practice and the flow of information in the application. A minimalist and clear design was used to display the data in a simple way, using apple design guides.
As for the process in Xcode, I started with the navigation and mock information to create the transitions, then make the API call and adjust the data model according to the requirements. Always maintaining an order of folders so that it is as organized as possible.

Create a model with encodable structure and data model using URLSession included in Swift language, which did not require the use of dependencies, taking advantage of the compiler to generate much of the code necessary to encode and decode data from a serialized format, such as JSON.

I chose MVVM because it seemed like an excellent opportunity to practice navigating with CORDINATORS, while in my opinion it is an architecture that allows you to perform more effective unit tests.

Use SOLID principles in your development code.

Use Postman to check the data call to the endPoint and check the types.

For the entire process, commits were made using "Sourcetree" for the git repository.

I decided to use the Cordinators for navigation because the displayed data does not strictly depend on each other, that is, they are independent.
In the development of functions and code it is about working under the premise of organization, cleanCode and S.O.L.I.D. principles, and generate a clear and divided structure for better updating and understanding, as well as the nomenclature of functions that are explicit enough to understand and Unit tests.



***

